// Determine whether incoming messages are ones we have interest in

use matrix_sdk::ruma::{
    api::client::receipt::create_receipt::v3::ReceiptType,
    events::{
        receipt::ReceiptThread,
        room::member::{MembershipChange, RoomMemberEventContent},
        room::message::{MessageType, Relation, RoomMessageEventContent},
        OriginalSyncMessageLikeEvent, OriginalSyncStateEvent,
    },
};
use matrix_sdk::{event_handler::Ctx, room::Room};

use crate::context::EventHelper;

pub mod greeter;
pub mod interface;

pub async fn message_handler(
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    room: Room,
    state: Ctx<EventHelper>,
) {
    let MessageType::Text(ref text_content) = ev.content.msgtype else {
        return;
    };

    let thread_root = if let Some(Relation::Thread(ref thread)) = ev.content.relates_to {
        ReceiptThread::Thread(thread.event_id.clone())
    } else {
        ReceiptThread::Main
    };

    room.send_single_receipt(ReceiptType::Read, thread_root, ev.event_id.clone())
        .await
        .unwrap();

    let body = &text_content.body;
    if body == "!f" || body.starts_with("!f ") {
        let owned = body.clone();
        let inner = state.0;
        interface::process(ev, owned, room, inner).await;
    }
}

pub async fn state_handler(ev: OriginalSyncStateEvent<RoomMemberEventContent>, room: Room) {
    room.send_single_receipt(ReceiptType::Read, ReceiptThread::Main, ev.event_id.clone())
        .await
        .unwrap();

    let (MembershipChange::Joined | MembershipChange::InvitationAccepted) = ev.membership_change()
    else {
        return;
    };

    greeter::process(ev, room).await;
}
